:begin
CREATE CONSTRAINT ON (node:Product) ASSERT (node.Id) IS UNIQUE;
CREATE CONSTRAINT ON (node:Category) ASSERT (node.Id) IS UNIQUE;
:commit
CALL db.awaitIndexes(300);
:begin
UNWIND [{Id:"1", properties:{Description:"234234324", Name:"Galaxy S10"}}, {Id:"2", properties:{Description:"Best seller", Name:"Iphone X"}}, {Id:"3", properties:{Description:"Thinking", Name:"Think Phone"}}, {Id:"4", properties:{Description:"34534ert", Name:"CertTin"}}, {Id:"SS-02", properties:{Description:"wrewr234", Name:"Galaxy B2"}}, {Id:"SS-T1", properties:{Description:"New tablet sam", Name:"Tablet Samsung B2"}}, {Id:"IR-01", properties:{Description:"Kim", Name:"Iris 1"}}] AS row
CREATE (n:Product{Id: row.Id}) SET n += row.properties;
UNWIND [{Id:"1", properties:{Name:"Smartphone"}}, {Id:"2", properties:{Name:"Tablet"}}, {Id:"3", properties:{Name:"Headphone"}}, {Id:"4", properties:{Name:"Laptop"}}] AS row
CREATE (n:Category{Id: row.Id}) SET n += row.properties;
:commit
:begin
UNWIND [{start: {Id:"1"}, end: {Id:"1"}, properties:{}}, {start: {Id:"1"}, end: {Id:"2"}, properties:{}}, {start: {Id:"1"}, end: {Id:"SS-02"}, properties:{}}, {start: {Id:"2"}, end: {Id:"SS-T1"}, properties:{}}, {start: {Id:"1"}, end: {Id:"IR-01"}, properties:{}}] AS row
MATCH (start:Category{Id: row.start.Id})
MATCH (end:Product{Id: row.end.Id})
CREATE (start)-[r:HAS_PRODUCT]->(end) SET r += row.properties;
UNWIND [{start: {Id:"1"}, end: {Id:"3"}, properties:{}}, {start: {Id:"1"}, end: {Id:"4"}, properties:{}}] AS row
MATCH (start:Category{Id: row.start.Id})
MATCH (end:Product{Id: row.end.Id})
CREATE (start)-[r:HAD_PRODUCT]->(end) SET r += row.properties;
:commit
