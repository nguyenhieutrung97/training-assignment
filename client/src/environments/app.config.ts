export const API_URI = {
  product: '/api/v1/products',
  category: '/api/v1/categories',
};
